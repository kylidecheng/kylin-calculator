/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __CALCULATOR_H__
#define __CALCULATOR_H__

#include <QWidget>
#include "plug_interface.h"

class Calculator : public QWidget , public KySmallPluginInterface
{
    Q_OBJECT
    Q_INTERFACES(KySmallPluginInterface)
    Q_PLUGIN_METADATA(IID SP_PLUGIN_IID FILE "Calculator.json")

public:
    const QString name() const;               /* 名称 */
    const QString nameCN() const;             /* 中文名 */
    const QString description() const;        /* 描述 */
    int sortNum() const;                      /* 排序 */
    QWidget *createWidget(QWidget *parent);   /* 创建窗体 */

protected:

private:

};


#endif
