/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <QApplication>
#include <QStringList>
#include <QTranslator>
#include <QLocale>
#include <QStandardPaths>
#include <QLibraryInfo>
#include <QDir>
#include <fcntl.h>
#include <syslog.h>
#include <QTranslator>
#include <QLocale>
#include <QDebug>
#include <QFile>
#include <QMutex>
#include <QDateTime>
#include <sys/inotify.h>

#include "calculator.h"
#include "data_warehouse.h"
#include "src/mainwindow.h"

const QString Calculator::name() const
{
    return "Calculator";
}

const QString Calculator::nameCN() const
{
    return QString("计算器");
}

const QString Calculator::description() const
{
    return QString("计算器");
}

int Calculator::sortNum() const
{
    return 3;
}

QWidget *Calculator::createWidget(QWidget *parent)
{
    /* 设置不跟随系统字号大小改变 */
    qApp->setProperty("noChangeSystemFontSize", true);

    /* 加载翻译文件 */
    QString tranPath("/usr/share/kylin-calculator/translations/");
    QTranslator *tran = new QTranslator;
    if (tran->load(QLocale() , QString("kylin-calculator") , QString("_") , tranPath)) {
        QApplication::installTranslator(tran);
    } else {
        qDebug() << "Waring : load translation file fail";
    }

    /* 注意 : 数据仓库需在界面前进行实例 , 界面实例时会访问数据仓库中的数据 */
    DataWarehouse::getInstance();

    /* 实例主界面 */
    MainWindow::getInstance();

    printf("Info : calculator plug start ...\n");

    return MainWindow::getInstance();
}
