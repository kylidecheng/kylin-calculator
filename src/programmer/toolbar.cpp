/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "toolbar.h"

ToolBar::ToolBar(QWidget *parent) : QWidget(parent)
{
    init();
    initLayout();
}

void ToolBar::init()
{
    // 创建控件
    m_btnASCII = new QPushButton();
    m_btnUnicode = new QPushButton();
    m_labCode = new QLabel();
    m_btnBin = new QPushButton();
    m_btnMS = new QPushButton();
    m_btnOtc = new QPushButton();
    m_btnDec = new QPushButton();
    m_btnHex = new QPushButton();
    m_labBase = new QLabel();
    m_boxDigit = new QComboBox();
    // 设置大小
    m_btnASCII->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_btnUnicode->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_btnBin->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_btnMS->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_btnOtc->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_btnDec->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_btnHex->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_boxDigit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    m_btnASCII->setFocusPolicy(Qt::NoFocus);
    m_btnUnicode->setFocusPolicy(Qt::NoFocus);
    m_btnBin->setFocusPolicy(Qt::NoFocus);
    m_btnMS->setFocusPolicy(Qt::NoFocus);
    m_btnOtc->setFocusPolicy(Qt::NoFocus);
    m_btnDec->setFocusPolicy(Qt::NoFocus);
    m_btnHex->setFocusPolicy(Qt::NoFocus);
    m_boxDigit->setFocusPolicy(Qt::NoFocus);
    m_btnASCII->setMinimumWidth(75);
    m_btnUnicode->setMinimumWidth(100);
    m_btnBin->setMinimumWidth(113);
    m_btnMS->setMinimumWidth(55);
    m_btnOtc->setMinimumWidth(53);
    m_btnDec->setMinimumWidth(53);
    m_btnHex->setMinimumWidth(53);
    m_boxDigit->setMinimumWidth(100);

    m_btnDec->setProperty("isImportant", true);
    m_btnDec->update();

    m_btnASCII->setFont(QFont("SourceHanSansCN"));
    m_btnUnicode->setFont(QFont("SourceHanSansCN"));
    m_btnBin->setFont(QFont("SourceHanSansCN"));
    m_btnMS->setFont(QFont("SourceHanSansCN"));
    m_btnOtc->setFont(QFont("SourceHanSansCN"));
    m_btnDec->setFont(QFont("SourceHanSansCN"));
    m_btnHex->setFont(QFont("SourceHanSansCN"));
    m_boxDigit->setFont(QFont("SourceHanSansCN"));

    // 按钮设置文本
    m_btnASCII->setText("ASCII");
    m_btnUnicode->setText("Unicode");
    m_btnBin->setText(tr("ShowBinary"));
    m_btnMS->setText("MS");
    m_btnOtc->setText("8");
    m_btnDec->setText("10");
    m_btnHex->setText("16");

    m_boxDigit->setStyleSheet("font-size:14px;QComboBox{font-size:14px;}");
    m_btnBin->setStyleSheet("font-size:14px;");
    m_btnASCII->setStyleSheet("font-size:14px;");
    m_btnUnicode->setStyleSheet("font-size:14px;");
    m_btnMS->setStyleSheet("font-size:14px;");
    m_btnOtc->setStyleSheet("font-size:14px;");
    m_btnDec->setStyleSheet("font-size:14px;");
    m_btnHex->setStyleSheet("font-size:14px;");

    // 连接槽
    connect(m_btnASCII, &QPushButton::clicked, this, &ToolBar::onClicked);
    connect(m_btnUnicode, &QPushButton::clicked, this, &ToolBar::onClicked);
    connect(m_btnOtc, &QPushButton::clicked, this, &ToolBar::onClicked);
    connect(m_btnDec, &QPushButton::clicked, this, &ToolBar::onClicked);
    connect(m_btnHex, &QPushButton::clicked, this, &ToolBar::onClicked);
    connect(m_btnMS, &QPushButton::clicked, this, &ToolBar::onClicked);
    connect(m_btnBin, &QPushButton::clicked, this, &ToolBar::onClicked);

    // 设置下拉列表
    m_boxDigit->addItem("QWORD"); // index = 0
    m_boxDigit->addItem("DWORD"); // index = 1
    m_boxDigit->addItem("WORD");  // index = 2
    m_boxDigit->addItem("BYTE");  // index = 3
    connect(m_boxDigit, SIGNAL(currentIndexChanged(int)), this, SLOT(valueChanged(int)));

    // 暂时现将MS功能隐藏
    m_btnMS->hide();
}

void ToolBar::initLayout()
{
    m_layout = new QHBoxLayout();
    m_layoutCode = new QHBoxLayout();
    m_layoutBase = new QHBoxLayout();

    m_layoutCode->addWidget(m_btnASCII);
    m_layoutCode->addWidget(m_btnUnicode);
    m_layoutCode->setSpacing(0);
    m_layoutCode->setMargin(0);
    m_labCode->setLayout(m_layoutCode);
    m_labCode->layout()->setSizeConstraint(QLayout::SetMinimumSize);

    m_layoutBase->addWidget(m_btnOtc);
    m_layoutBase->addWidget(m_btnDec);
    m_layoutBase->addWidget(m_btnHex);
    m_layoutBase->setSpacing(0);
    m_layoutBase->setMargin(0);
    m_labBase->setLayout(m_layoutBase);
    m_labBase->layout()->setSizeConstraint(QLayout::SetMinimumSize);

    m_layout->addWidget(m_labCode, 367);
    m_layout->addStretch(45);
    m_layout->addWidget(m_boxDigit, 305);
    m_layout->addStretch(45);
    m_layout->addWidget(m_btnBin, 283);
    m_layout->addStretch(45);
    m_layout->addWidget(m_btnMS, 112);
    m_layout->addStretch(56);
    m_layout->addWidget(m_labBase, 333);
    m_layout->setMargin(0);

    this->setLayout(m_layout);
}

void ToolBar::setBaseEnabled(bool state)
{
    m_btnOtc->setEnabled(state);
    m_btnDec->setEnabled(state);
    m_btnHex->setEnabled(state);
    m_btnASCII->setEnabled(state);
    m_btnUnicode->setEnabled(state);
    m_boxDigit->setEnabled(state);
}

void ToolBar::onClicked()
{
    QPushButton *btn = qobject_cast<QPushButton *>(sender());

    // 设置特殊按钮组的状态
    if (m_btnASCII == btn) {
        if (m_isASCII) {
            // 按钮已经被选中
            emit sigBtnClicked(QString("cancel"));
            m_isASCII = false;
            m_btnASCII->setProperty("isImportant", false);
        } else {
            // 按钮未被选中,发送信号
            emit sigBtnClicked(btn->text());
            m_isASCII = true;
            m_isUnicode = false;
            m_btnASCII->setProperty("isImportant", true);
            m_btnUnicode->setProperty("isImportant", false);
        }
        m_btnASCII->update();
        m_btnUnicode->update();
        return;
    }
    if (m_btnUnicode == btn) {
        if (m_isUnicode) {
            // 按钮已经被选中
            emit sigBtnClicked(QString("cancel"));
            m_isUnicode = false;
            m_btnUnicode->setProperty("isImportant", false);
        } else {
            // 按钮未被选中
            emit sigBtnClicked(btn->text());
            m_isASCII = false;
            m_isUnicode = true;
            m_btnASCII->setProperty("isImportant", false);
            m_btnUnicode->setProperty("isImportant", true);
        }
        m_btnASCII->update();
        m_btnUnicode->update();
        return;
    }

    emit sigBtnClicked(btn->text());

    if (m_btnOtc == btn) {
        m_btnOtc->setProperty("isImportant", true);
        m_btnDec->setProperty("isImportant", false);
        m_btnHex->setProperty("isImportant", false);
    }
    if (m_btnDec == btn) {
        m_btnOtc->setProperty("isImportant", false);
        m_btnDec->setProperty("isImportant", true);
        m_btnHex->setProperty("isImportant", false);
    }
    if (m_btnHex == btn) {
        m_btnOtc->setProperty("isImportant", false);
        m_btnDec->setProperty("isImportant", false);
        m_btnHex->setProperty("isImportant", true);
    }
    m_btnOtc->update();
    m_btnDec->update();
    m_btnHex->update();

    // 设置m_btnBin文本
    if (QString(tr("ShowBinary")) == btn->text()) {
        btn->setText(tr("HideBinary"));
    } else if (QString(tr("HideBinary")) == btn->text()) {
        btn->setText(tr("ShowBinary"));
    }
}

void ToolBar::valueChanged(int index)
{
    emit sigBoxValueChanged(index);
}
