/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BASEBINARY_H
#define BASEBINARY_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QString>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QList>

class BaseBinary : public QWidget
{
    Q_OBJECT
public:
    explicit BaseBinary(QString value);

    // 设置显示
    // value:一个四位的2进制字符串1，将这个value设置到四个按钮上
    void setData(QString value);

    // 返回当前显示的数据
    QString data();

    // 全部设置为0
    void clear();

private:

    QString m_labValue;
    // 设置二进制位
    QString m_flag0 = QString("0");
    QString m_flag1 = QString("1");

    QList<QPushButton *> m_btnList;
    QLabel *m_lab;
    QHBoxLayout *m_hlayout;
    QHBoxLayout* m_spaceLayout;
    QVBoxLayout *m_vlayout;

    void init(void);
    void initLayout(void);

signals:

public slots:
    void onClicked(void);

};

#endif // BASEBINARY_H
