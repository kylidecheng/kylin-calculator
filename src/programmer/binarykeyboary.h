/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef BINARYKEYBOARY_H
#define BINARYKEYBOARY_H

#include <QWidget>
#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QString>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <QList>

#include "basebinary.h"

class BinaryKeyboary : public QWidget
{
    Q_OBJECT
public:
    explicit BinaryKeyboary();

    // 设置需要显示的二进制
    void setData(QString value);

    // 返回当前显示的数据
    QString data();

    void clear();

private:
    // 存放显示四位二进制的基本控件指针
    QList<BaseBinary *> m_baseList;

    QHBoxLayout* m_topLayoout;
    QHBoxLayout* m_bottomLayout;
    QVBoxLayout* m_vlayout;

    void init(void);


signals:

};

#endif // BINARYKEYBOARY_H
