<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>Calc</name>
    <message>
        <source>Divisor cannot be 0!</source>
        <translation>دىسنېيور 0 بولالمايدۇ!</translation>
    </message>
    <message>
        <source>The expression is empty!</source>
        <translation>چىراي ئىپادىسى قۇرۇق!</translation>
    </message>
    <message>
        <source>Missing left parenthesis!</source>
        <translation>سول قادام يوقاپ كەتتى!</translation>
    </message>
    <message>
        <source>Miss operand!</source>
        <translation>ئوپېرا خانقىز!</translation>
    </message>
    <message>
        <source>Expression error!</source>
        <translation>چىراي ئىپادىسى خاتالىقى!</translation>
    </message>
    <message>
        <source>The value is too large!</source>
        <translation>قىممىتى بەك چوڭ ئىكەن!</translation>
    </message>
    <message>
        <source>Operator undefined!</source>
        <translation>تىجارەتچىنىڭ بېكىتىلمىگەن!</translation>
    </message>
    <message>
        <source>Right operand error!</source>
        <translation>توغرا ئوپېرا خاتالىقى!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <source>Unit converter</source>
        <translation>بىرلىك ئايلاندۇرغۇچ</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>پېرېۋوت نىسبىتى</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>ھېسابلىغۇچ</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ئىلمىي</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <source>standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ئىلمىي</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Copy</source>
        <translation>كۆچۈرۈش</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>چاپلاش</translation>
    </message>
    <message>
        <source>input too long</source>
        <translation>خەت كىرگۈزۈش بەك ئۇزۇن</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>پېرېۋوت نىسبىتى</translation>
    </message>
    <message>
        <source>Input error!</source>
        <translation>كىرگۈزۈش خاتالىقى!</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>خاتالىق!</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <source>calculator</source>
        <translation>ھېسابلىغۇچ</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>ھېسابلىغۇچ</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ئىلمىي</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <source>input too long!</source>
        <translation>بەك ئۇزۇن خەت كىرگۈزۈڭ!</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <source>Input error!</source>
        <translation>كىرگۈزۈش خاتالىقى!</translation>
    </message>
    <message>
        <source>HideBinary</source>
        <translation>HideBinary</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>پېرېۋوت نىسبىتى</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ئەڭ چوڭ چەككە</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <source>StayTop</source>
        <translation>StayTop</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ئىلمىي</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>ئىلمىي تەتقىقات</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>پروگراممېر</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>HideBinary</source>
        <translation>HideBinary</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <source>US Dollar</source>
        <translation>ئامېرىكا دوللىرى</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>خاتالىق!</translation>
    </message>
    <message>
        <source>Rate update</source>
        <translation>نىسبىتى يېڭىلاش</translation>
    </message>
    <message>
        <source>Chinese Yuan</source>
        <translation>خەنزۇچە يۈەن</translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <source>cancel</source>
        <translation type="vanished">ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <source>currency</source>
        <translation type="vanished">پۇل</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Auto</source>
        <translation type="vanished">ئاپتۇماتىك</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">قاراڭغۇلۇق</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">تىزىملىك</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>چېكىنىش</translation>
    </message>
    <message>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">نۇر</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>ئۇسلۇب</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>پېرېۋوت نىسبىتى</translation>
    </message>
    <message>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>ھېسابلىغۇچ Qt5 ئاساسىدىكى يېنىك ھېسابلىغۇچ بولۇپ، ئۆلچەملىك ھېسابلاش، ئىلمىي ھېسابلاش ۋە پېرېۋوت نىسبىتىنى ئايلاندۇرۇشنى تەمىنلەيدۇ.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">مۇلازىمەت &gt; قوللاش: </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>نەشرى: </translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>ئۆلچەم</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>ھېسابلىغۇچ</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>ئىلمىي تەتقىقات</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>پروگراممېر</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
