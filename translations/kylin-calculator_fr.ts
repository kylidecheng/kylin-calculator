<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>L’expression est vide !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>Erreur d’expression !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>Parenthèse gauche manquante !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>La valeur est trop importante !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>Miss opérande !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>Opérateur indéfini !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>Le diviseur ne peut pas être égal à 0 !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>Erreur d’opérande droit !</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>Calculatrice</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>scientifique</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>Convertisseur d’unités</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>taux de change</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>scientifique</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="249"/>
        <source>Calculator</source>
        <translation>Calculatrice</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1321"/>
        <source>standard</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1303"/>
        <source>calculator</source>
        <translation>calculatrice</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="276"/>
        <source>Copy</source>
        <translation>Copier</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Paste</source>
        <translation>Pâte</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>input too long</source>
        <translation>entrée trop longue</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="77"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>scientific</source>
        <translation>scientifique</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1338"/>
        <source>exchange rate</source>
        <translation>taux de change</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <location filename="../src/mainwindow.cpp" line="831"/>
        <source>Error!</source>
        <translation>Erreur!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="835"/>
        <source>Input error!</source>
        <translation>Erreur de saisie !</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>Entrée trop longue !</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="86"/>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="87"/>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="88"/>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="89"/>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="232"/>
        <location filename="../src/programmer/programmodel.cpp" line="316"/>
        <source>Input error!</source>
        <translation>Erreur de saisie !</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="398"/>
        <source>ShowBinary</source>
        <translation>ShowBinary (Binaire)</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="409"/>
        <source>HideBinary</source>
        <translation>MasquerBinaire</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Calculator</source>
        <translation type="vanished">计算器</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <location filename="../src/scientificmodel.cpp" line="196"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="268"/>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="269"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="271"/>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="272"/>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="273"/>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="274"/>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="276"/>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="277"/>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="278"/>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="279"/>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="281"/>
        <location filename="../src/scientificmodel.cpp" line="1019"/>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="282"/>
        <location filename="../src/scientificmodel.cpp" line="1020"/>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="283"/>
        <location filename="../src/scientificmodel.cpp" line="1021"/>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="284"/>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="286"/>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="287"/>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="288"/>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="289"/>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1012"/>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1013"/>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1014"/>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="288"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="289"/>
        <source>Scientific</source>
        <translation>Scientifique</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="55"/>
        <location filename="../src/titlebar.cpp" line="70"/>
        <location filename="../src/titlebar.cpp" line="256"/>
        <source>standard</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="56"/>
        <location filename="../src/titlebar.cpp" line="268"/>
        <source>scientific</source>
        <translation>scientifique</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="290"/>
        <source>Exchange Rate</source>
        <translation>Taux de change</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Programmer</source>
        <translation>Programmeur</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="316"/>
        <source>StayTop</source>
        <translation>StayTop (en anglais seulement)</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="426"/>
        <source>Restore</source>
        <translation>Restaurer</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="120"/>
        <location filename="../src/titlebar.cpp" line="317"/>
        <source>Minimize</source>
        <translation>Minimiser</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="121"/>
        <location filename="../src/titlebar.cpp" line="318"/>
        <location filename="../src/titlebar.cpp" line="417"/>
        <source>Maximize</source>
        <translation>Maximiser</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="122"/>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="63"/>
        <location filename="../src/programmer/toolbar.cpp" line="207"/>
        <location filename="../src/programmer/toolbar.cpp" line="210"/>
        <source>ShowBinary</source>
        <translation>ShowBinary (Binaire)</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="208"/>
        <location filename="../src/programmer/toolbar.cpp" line="209"/>
        <source>HideBinary</source>
        <translation>MasquerBinaire</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="82"/>
        <source>Rate update</source>
        <translation>Mise à jour des tarifs</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="97"/>
        <source>Chinese Yuan</source>
        <translation>Yuan chinois</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="104"/>
        <source>US Dollar</source>
        <translation>Dollar</translation>
    </message>
    <message>
        <source>UAE Dirham</source>
        <translation type="vanished">Dirham des Émirats arabes unis</translation>
    </message>
    <message>
        <source>Argentinian peso</source>
        <translation type="vanished">Peso argentin</translation>
    </message>
    <message>
        <source>Australian Dollar</source>
        <translation type="vanished">Dollar australien</translation>
    </message>
    <message>
        <source>Bulgarian Lev</source>
        <translation type="vanished">Lev bulgare</translation>
    </message>
    <message>
        <source>Bahraini Dinar</source>
        <translation type="vanished">Dinar bahreïni</translation>
    </message>
    <message>
        <source>Brunei Dollar</source>
        <translation type="vanished">Brunei Dollar</translation>
    </message>
    <message>
        <source>Brazilian Real</source>
        <translation type="vanished">Réal brésilien</translation>
    </message>
    <message>
        <source>Bahaman Dollar</source>
        <translation type="vanished">Dollar des Bahamas</translation>
    </message>
    <message>
        <source>Botswana Pula</source>
        <translation type="vanished">Botswana Pula</translation>
    </message>
    <message>
        <source>Canadian Dollar</source>
        <translation type="vanished">Dollar canadien</translation>
    </message>
    <message>
        <source>CFA Franc</source>
        <translation type="vanished">Franc CFA</translation>
    </message>
    <message>
        <source>Swiss Franc</source>
        <translation type="vanished">Franc suisse</translation>
    </message>
    <message>
        <source>Chilean Peso</source>
        <translation type="vanished">Peso chilien</translation>
    </message>
    <message>
        <source>Colombian Peso</source>
        <translation type="vanished">Peso colombien</translation>
    </message>
    <message>
        <source>Czech Koruna</source>
        <translation type="vanished">Couronne tchèque</translation>
    </message>
    <message>
        <source>Danish Krone</source>
        <translation type="vanished">Couronne danoise</translation>
    </message>
    <message>
        <source>Dominican peso</source>
        <translation type="vanished">Peso dominicain</translation>
    </message>
    <message>
        <source>Algerian Dinar</source>
        <translation type="vanished">Dinar algérien</translation>
    </message>
    <message>
        <source>Estonian Kroon</source>
        <translation type="vanished">Couronne estonienne</translation>
    </message>
    <message>
        <source>Egyptian pound</source>
        <translation type="vanished">Livre égyptienne</translation>
    </message>
    <message>
        <source>Euro</source>
        <translation type="vanished">Euro</translation>
    </message>
    <message>
        <source>Fijian dollar</source>
        <translation type="vanished">Dollar fidjien</translation>
    </message>
    <message>
        <source>Pound Sterling</source>
        <translation type="vanished">Livre sterling</translation>
    </message>
    <message>
        <source>Guatemalan Quetzal</source>
        <translation type="vanished">Quetzal guatémaltèque</translation>
    </message>
    <message>
        <source>Hong Kong Dollar</source>
        <translation type="vanished">Hong Kong Dollar</translation>
    </message>
    <message>
        <source>Croatian Kuna</source>
        <translation type="vanished">Kuna croate</translation>
    </message>
    <message>
        <source>Hungarian Forint</source>
        <translation type="vanished">Forint hongrois</translation>
    </message>
    <message>
        <source>Indonesian Rupiah</source>
        <translation type="vanished">Roupie indonésienne</translation>
    </message>
    <message>
        <source>Israeli New Shekel</source>
        <translation type="vanished">Nouveau shekel israélien</translation>
    </message>
    <message>
        <source>Indian Rupee</source>
        <translation type="vanished">Roupie indienne</translation>
    </message>
    <message>
        <source>Iranian Rial</source>
        <translation type="vanished">Rial iranien</translation>
    </message>
    <message>
        <source>Icelandic Krona</source>
        <translation type="vanished">Couronne islandaise</translation>
    </message>
    <message>
        <source>Japanese Yen</source>
        <translation type="vanished">Yen japonais</translation>
    </message>
    <message>
        <source>South Korean Won</source>
        <translation type="vanished">Won sud-coréen</translation>
    </message>
    <message>
        <source>Kuwaiti Dinar</source>
        <translation type="vanished">Dinar koweïtien</translation>
    </message>
    <message>
        <source>Kazakhstani Tenge</source>
        <translation type="vanished">Tenge kazakh</translation>
    </message>
    <message>
        <source>Sri Lankan Rupee</source>
        <translation type="vanished">Roupie sri-lankaise</translation>
    </message>
    <message>
        <source>Lithuanian Litas</source>
        <translation type="vanished">Litas lituanien</translation>
    </message>
    <message>
        <source>Latvian Lats</source>
        <translation type="vanished">Lats lettons</translation>
    </message>
    <message>
        <source>Libyan Dinar</source>
        <translation type="vanished">Dinar libyen</translation>
    </message>
    <message>
        <source>Mauritian Rupee</source>
        <translation type="vanished">Roupie mauricienne</translation>
    </message>
    <message>
        <source>Maldivian Rupee</source>
        <translation type="vanished">Roupie des Maldives</translation>
    </message>
    <message>
        <source>Mexican Peso</source>
        <translation type="vanished">Peso mexicain</translation>
    </message>
    <message>
        <source>Malaysian Ringgit</source>
        <translation type="vanished">Ringgit malaisien</translation>
    </message>
    <message>
        <source>Norwegian Krone</source>
        <translation type="vanished">Couronne norvégienne</translation>
    </message>
    <message>
        <source>Nepalese Rupee</source>
        <translation type="vanished">Roupie népalaise</translation>
    </message>
    <message>
        <source>New Zealand Dollar</source>
        <translation type="vanished">Dollar néo-zélandais</translation>
    </message>
    <message>
        <source>Omani Rial</source>
        <translation type="vanished">Rial omanais</translation>
    </message>
    <message>
        <source>Panamanian balbos</source>
        <translation type="vanished">Balbos panaméens</translation>
    </message>
    <message>
        <source>Peruvian Nuevo Sol</source>
        <translation type="vanished">Nuevo Sol péruvien</translation>
    </message>
    <message>
        <source>Philippine Peso</source>
        <translation type="vanished">Peso philippin</translation>
    </message>
    <message>
        <source>Pakistani Rupee</source>
        <translation type="vanished">Roupie pakistanaise</translation>
    </message>
    <message>
        <source>Polish Zloty</source>
        <translation type="vanished">Zloty polonais</translation>
    </message>
    <message>
        <source>Paraguayan Guaran</source>
        <translation type="vanished">Guaran paraguayen</translation>
    </message>
    <message>
        <source>Qatari Riyal</source>
        <translation type="vanished">Rial qatari</translation>
    </message>
    <message>
        <source>New Romanian Leu</source>
        <translation type="vanished">Nouveau Leu roumain</translation>
    </message>
    <message>
        <source>Russian Rouble</source>
        <translation type="vanished">Rouble russe</translation>
    </message>
    <message>
        <source>Saudi Riyal</source>
        <translation type="vanished">Rial saoudien</translation>
    </message>
    <message>
        <source>Swedish Krona</source>
        <translation type="vanished">Couronne suédoise</translation>
    </message>
    <message>
        <source>Singapore Dollar</source>
        <translation type="vanished">Dollar de Singapour</translation>
    </message>
    <message>
        <source>Thai Baht</source>
        <translation type="vanished">Baht thaïlandais</translation>
    </message>
    <message>
        <source>Tunisian Dinar</source>
        <translation type="vanished">Dinar tunisien</translation>
    </message>
    <message>
        <source>New Turkish Lira</source>
        <translation type="vanished">Nouvelle livre turque</translation>
    </message>
    <message>
        <source>T&amp;T Dollar (TTD)</source>
        <translation type="vanished">Dollar T&amp;T (TTD)</translation>
    </message>
    <message>
        <source>Taiwan Dollar</source>
        <translation type="vanished">Dollar de Taïwan</translation>
    </message>
    <message>
        <source>Ukrainian Hryvnia</source>
        <translation type="vanished">Hryvnia ukrainienne</translation>
    </message>
    <message>
        <source>Uruguayan Peso</source>
        <translation type="vanished">Peso uruguayen</translation>
    </message>
    <message>
        <source>Venezuelan Bolívar</source>
        <translation type="vanished">Bolívar vénézuélien</translation>
    </message>
    <message>
        <source>South African Rand</source>
        <translation type="vanished">Rand sud-africain</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="504"/>
        <source>Error!</source>
        <translation>Erreur!</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>Scientifique</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>Taux de change</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>Programmeur</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>Thème</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>Environ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>Démissionner</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>Calculator est une calculatrice légère basée sur Qt5, qui fournit un calcul standard, un calcul scientifique et une conversion de taux de change.</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>Calculatrice</translation>
    </message>
</context>
</TS>
