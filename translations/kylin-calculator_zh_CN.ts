<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>表达式为空!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>表达式错误!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>缺少左括号!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>数值过大!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>缺少操作数!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>未定义操作符!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>除数不能为0!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>右操作数错误!</translation>
    </message>
    <message>
        <source>The shifted right operand is negative!</source>
        <translation type="vanished">移位操作右值不能为负数!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>计算器</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>换算器</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>汇率</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="249"/>
        <source>Calculator</source>
        <translation>计算器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1321"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1303"/>
        <source>calculator</source>
        <translation>计算器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="276"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>input too long</source>
        <translation>输入过长</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="77"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1338"/>
        <source>exchange rate</source>
        <translation>汇率</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <location filename="../src/mainwindow.cpp" line="831"/>
        <source>Error!</source>
        <translation>错误!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="835"/>
        <source>Input error!</source>
        <translation>输入错误!</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>输入过长!</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="86"/>
        <source>Move X 1 bit to the right</source>
        <translation>将 X 向右移 1 位</translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="87"/>
        <source>Move X 1 bit to the left</source>
        <translation>将 X 向左移 1 位</translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="88"/>
        <source>Move X to the right by y bits</source>
        <translation>将 X 向右移 y 位</translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="89"/>
        <source>Move X to the left by y bits</source>
        <translation>将 X 向左移 y 位</translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="232"/>
        <location filename="../src/programmer/programmodel.cpp" line="316"/>
        <source>Input error!</source>
        <translation>输入错误!</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="398"/>
        <source>ShowBinary</source>
        <translation>显示二进制</translation>
    </message>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="409"/>
        <source>HideBinary</source>
        <translation>隐藏二进制</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <location filename="../src/scientificmodel.cpp" line="196"/>
        <source>Clear</source>
        <translation>清除</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="268"/>
        <source>Change some keys to interleaving functions</source>
        <translation>将某些按键更改为交错函数</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="269"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="271"/>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation>计算所显示数值的倒数</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="272"/>
        <source>Square the displayed value</source>
        <translation>对所显示的值求平方值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="273"/>
        <source>Calculate the cubic value of the displayed value</source>
        <translation>对显示的值求立方值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="274"/>
        <source>Calculate the displayed value power of the next input value</source>
        <translation>计算下一个输入值的所显示值次方</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="276"/>
        <source>Calculate the factorial of the displayed value</source>
        <translation>计算所显示数值的阶乘</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="277"/>
        <source>Square root of the displayed square root</source>
        <translation>对所显示的开平方</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="278"/>
        <source>Cubic representation of the displayed value</source>
        <translation>对显示的值开立方</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="279"/>
        <source> Calculate the displayed value to the y-th power</source>
        <translation>计算所显示的值开y次方</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="281"/>
        <location filename="../src/scientificmodel.cpp" line="1019"/>
        <source>Calculate the sine value of the displayed value</source>
        <translation>计算所显示数值的正弦值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="282"/>
        <location filename="../src/scientificmodel.cpp" line="1020"/>
        <source>Calculate the cosine value of the displayed value</source>
        <translation>计算所显示数值的余弦值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="283"/>
        <location filename="../src/scientificmodel.cpp" line="1021"/>
        <source>Calculate the tangent value of the displayed value</source>
        <translation>计算所显示数值的正切值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="284"/>
        <source>Calculate the index value based on the displayed value</source>
        <translation>计算以显示数值为底数的指数值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="286"/>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation>在度数和弧数之间切换（点击切换）</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="287"/>
        <source>Input pi (3.141596...)</source>
        <translation>输入圆周率（3.141596…）</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="288"/>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation>输入e（e的值为e≈2.71828 18284 59…）</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="289"/>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation>计算所显示数值的自然对数</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1012"/>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation>计算所显示数值的反正弦值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1013"/>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation>计算所显示数值的反余弦值</translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1014"/>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation>计算所显示数值的反正切值</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>FuncList</source>
        <translation type="vanished">功能列表</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="288"/>
        <source>Standard</source>
        <translation>计算器—标准</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="289"/>
        <source>Scientific</source>
        <translation>计算器—科学</translation>
    </message>
    <message>
        <source>standard </source>
        <translation type="vanished">标准 </translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="55"/>
        <location filename="../src/titlebar.cpp" line="70"/>
        <location filename="../src/titlebar.cpp" line="256"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="56"/>
        <location filename="../src/titlebar.cpp" line="268"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <source>scientific </source>
        <translation type="vanished">科学 </translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="290"/>
        <source>Exchange Rate</source>
        <translation>计算器—汇率</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Programmer</source>
        <translation>计算器—程序员</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="316"/>
        <source>StayTop</source>
        <translation>置顶</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="426"/>
        <source>Restore</source>
        <translation>还原</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="120"/>
        <location filename="../src/titlebar.cpp" line="317"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="121"/>
        <location filename="../src/titlebar.cpp" line="318"/>
        <location filename="../src/titlebar.cpp" line="417"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="122"/>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="63"/>
        <location filename="../src/programmer/toolbar.cpp" line="207"/>
        <location filename="../src/programmer/toolbar.cpp" line="210"/>
        <source>ShowBinary</source>
        <translation>显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="208"/>
        <location filename="../src/programmer/toolbar.cpp" line="209"/>
        <source>HideBinary</source>
        <translation>隐藏二进制</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="82"/>
        <source>Rate update</source>
        <translation>汇率更新</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="97"/>
        <source>Chinese Yuan</source>
        <translation>人民币</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="104"/>
        <source>US Dollar</source>
        <translation>美元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="504"/>
        <source>Error!</source>
        <translation>错误!</translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <source>currency</source>
        <translation type="vanished">货币</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Menu</source>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>选项</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>汇率</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>程序员</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">浅色</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">深色</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>版本号: </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>计算器是一款基于qt5开发的轻量级计算器，提供标准计算，科学计算和汇率换算。</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>Support: support@kylinos.cn</source>
        <translation type="vanished">支持：support@kylinos.cn</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>计算器</translation>
    </message>
</context>
</TS>
