<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>¡La expresión está vacía!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>¡Error de expresión!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>¡Falta el paréntesis izquierdo!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>¡El valor es demasiado grande!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>¡Señorita operando!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>¡Operador indefinido!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>¡El divisor no puede ser 0!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>¡Error de operando correcto!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>Calculadora</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>estándar</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>científico</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>Convertidor de unidades</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>tipo de cambio</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>estándar</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>científico</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="249"/>
        <source>Calculator</source>
        <translation>Calculadora</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1321"/>
        <source>standard</source>
        <translation>estándar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1303"/>
        <source>calculator</source>
        <translation>calculadora</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="276"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>input too long</source>
        <translation>Entrada demasiado larga</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="77"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>scientific</source>
        <translation>científico</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1338"/>
        <source>exchange rate</source>
        <translation>tipo de cambio</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <location filename="../src/mainwindow.cpp" line="831"/>
        <source>Error!</source>
        <translation>¡Error!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="835"/>
        <source>Input error!</source>
        <translation>¡Error de entrada!</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>¡Entrada demasiado larga!</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="86"/>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="87"/>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="88"/>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="89"/>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="232"/>
        <location filename="../src/programmer/programmodel.cpp" line="316"/>
        <source>Input error!</source>
        <translation>¡Error de entrada!</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="398"/>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="409"/>
        <source>HideBinary</source>
        <translation>HideBinary (en inglés)</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Calculator</source>
        <translation type="vanished">计算器</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <location filename="../src/scientificmodel.cpp" line="196"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="268"/>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="269"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="271"/>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="272"/>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="273"/>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="274"/>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="276"/>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="277"/>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="278"/>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="279"/>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="281"/>
        <location filename="../src/scientificmodel.cpp" line="1019"/>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="282"/>
        <location filename="../src/scientificmodel.cpp" line="1020"/>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="283"/>
        <location filename="../src/scientificmodel.cpp" line="1021"/>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="284"/>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="286"/>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="287"/>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="288"/>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="289"/>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1012"/>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1013"/>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1014"/>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="288"/>
        <source>Standard</source>
        <translation>Estándar</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="289"/>
        <source>Scientific</source>
        <translation>Científico</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="55"/>
        <location filename="../src/titlebar.cpp" line="70"/>
        <location filename="../src/titlebar.cpp" line="256"/>
        <source>standard</source>
        <translation>estándar</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="56"/>
        <location filename="../src/titlebar.cpp" line="268"/>
        <source>scientific</source>
        <translation>científico</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="290"/>
        <source>Exchange Rate</source>
        <translation>Tipo de cambio</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Programmer</source>
        <translation>Programador</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="316"/>
        <source>StayTop</source>
        <translation>StayTop</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="426"/>
        <source>Restore</source>
        <translation>Restaurar</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="120"/>
        <location filename="../src/titlebar.cpp" line="317"/>
        <source>Minimize</source>
        <translation>Minimizar</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="121"/>
        <location filename="../src/titlebar.cpp" line="318"/>
        <location filename="../src/titlebar.cpp" line="417"/>
        <source>Maximize</source>
        <translation>Maximizar</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="122"/>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="63"/>
        <location filename="../src/programmer/toolbar.cpp" line="207"/>
        <location filename="../src/programmer/toolbar.cpp" line="210"/>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="208"/>
        <location filename="../src/programmer/toolbar.cpp" line="209"/>
        <source>HideBinary</source>
        <translation>HideBinary (en inglés)</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="82"/>
        <source>Rate update</source>
        <translation>Actualización de tarifas</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="97"/>
        <source>Chinese Yuan</source>
        <translation>Yuan chino</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="104"/>
        <source>US Dollar</source>
        <translation>Dólar de EE.UU</translation>
    </message>
    <message>
        <source>UAE Dirham</source>
        <translation type="vanished">Dírham de los Emiratos Árabes Unidos</translation>
    </message>
    <message>
        <source>Argentinian peso</source>
        <translation type="vanished">Peso argentino</translation>
    </message>
    <message>
        <source>Australian Dollar</source>
        <translation type="vanished">Dólar australiano</translation>
    </message>
    <message>
        <source>Bulgarian Lev</source>
        <translation type="vanished">Lev búlgaro</translation>
    </message>
    <message>
        <source>Bahraini Dinar</source>
        <translation type="vanished">Dinar bahreiní</translation>
    </message>
    <message>
        <source>Brunei Dollar</source>
        <translation type="vanished">Dólar de Brunéi</translation>
    </message>
    <message>
        <source>Brazilian Real</source>
        <translation type="vanished">Real brasileño</translation>
    </message>
    <message>
        <source>Bahaman Dollar</source>
        <translation type="vanished">Dólar de las Bahamas</translation>
    </message>
    <message>
        <source>Botswana Pula</source>
        <translation type="vanished">Botswana Pula</translation>
    </message>
    <message>
        <source>Canadian Dollar</source>
        <translation type="vanished">Dólar canadiense</translation>
    </message>
    <message>
        <source>CFA Franc</source>
        <translation type="vanished">Franco CFA</translation>
    </message>
    <message>
        <source>Swiss Franc</source>
        <translation type="vanished">Franco suizo</translation>
    </message>
    <message>
        <source>Chilean Peso</source>
        <translation type="vanished">Peso Chileno</translation>
    </message>
    <message>
        <source>Colombian Peso</source>
        <translation type="vanished">Peso Colombiano</translation>
    </message>
    <message>
        <source>Czech Koruna</source>
        <translation type="vanished">Corona checa</translation>
    </message>
    <message>
        <source>Danish Krone</source>
        <translation type="vanished">Corona danesa</translation>
    </message>
    <message>
        <source>Dominican peso</source>
        <translation type="vanished">Peso dominicano</translation>
    </message>
    <message>
        <source>Algerian Dinar</source>
        <translation type="vanished">Dinar argelino</translation>
    </message>
    <message>
        <source>Estonian Kroon</source>
        <translation type="vanished">Coronas estonias</translation>
    </message>
    <message>
        <source>Egyptian pound</source>
        <translation type="vanished">Libra egipcia</translation>
    </message>
    <message>
        <source>Euro</source>
        <translation type="vanished">Euro</translation>
    </message>
    <message>
        <source>Fijian dollar</source>
        <translation type="vanished">Dólar fiyiano</translation>
    </message>
    <message>
        <source>Pound Sterling</source>
        <translation type="vanished">Libra esterlina</translation>
    </message>
    <message>
        <source>Guatemalan Quetzal</source>
        <translation type="vanished">Quetzal guatemalteco</translation>
    </message>
    <message>
        <source>Hong Kong Dollar</source>
        <translation type="vanished">Dólar de Hong Kong</translation>
    </message>
    <message>
        <source>Croatian Kuna</source>
        <translation type="vanished">Kuna croata</translation>
    </message>
    <message>
        <source>Hungarian Forint</source>
        <translation type="vanished">Florín húngaro</translation>
    </message>
    <message>
        <source>Indonesian Rupiah</source>
        <translation type="vanished">Rupia indonesia</translation>
    </message>
    <message>
        <source>Israeli New Shekel</source>
        <translation type="vanished">Nuevo Shekel israelí</translation>
    </message>
    <message>
        <source>Indian Rupee</source>
        <translation type="vanished">Rupia india</translation>
    </message>
    <message>
        <source>Iranian Rial</source>
        <translation type="vanished">Rial iraní</translation>
    </message>
    <message>
        <source>Icelandic Krona</source>
        <translation type="vanished">Corona islandesa</translation>
    </message>
    <message>
        <source>Japanese Yen</source>
        <translation type="vanished">Yen japonés</translation>
    </message>
    <message>
        <source>South Korean Won</source>
        <translation type="vanished">Won surcoreano</translation>
    </message>
    <message>
        <source>Kuwaiti Dinar</source>
        <translation type="vanished">Dinar kuwaití</translation>
    </message>
    <message>
        <source>Kazakhstani Tenge</source>
        <translation type="vanished">Tenge kazajo</translation>
    </message>
    <message>
        <source>Sri Lankan Rupee</source>
        <translation type="vanished">Rupia de Sri Lanka</translation>
    </message>
    <message>
        <source>Lithuanian Litas</source>
        <translation type="vanished">Litas lituanas</translation>
    </message>
    <message>
        <source>Latvian Lats</source>
        <translation type="vanished">Lats letones</translation>
    </message>
    <message>
        <source>Libyan Dinar</source>
        <translation type="vanished">Dinar libio</translation>
    </message>
    <message>
        <source>Mauritian Rupee</source>
        <translation type="vanished">Rupia mauriciana</translation>
    </message>
    <message>
        <source>Maldivian Rupee</source>
        <translation type="vanished">Rupia de Maldivas</translation>
    </message>
    <message>
        <source>Mexican Peso</source>
        <translation type="vanished">Peso Mexicano</translation>
    </message>
    <message>
        <source>Malaysian Ringgit</source>
        <translation type="vanished">Ringgit malayo</translation>
    </message>
    <message>
        <source>Norwegian Krone</source>
        <translation type="vanished">Corona noruega</translation>
    </message>
    <message>
        <source>Nepalese Rupee</source>
        <translation type="vanished">Rupia nepalí</translation>
    </message>
    <message>
        <source>New Zealand Dollar</source>
        <translation type="vanished">Dólar neozelandés</translation>
    </message>
    <message>
        <source>Omani Rial</source>
        <translation type="vanished">Rial omaní</translation>
    </message>
    <message>
        <source>Panamanian balbos</source>
        <translation type="vanished">Balbos panameños</translation>
    </message>
    <message>
        <source>Peruvian Nuevo Sol</source>
        <translation type="vanished">Nuevo Sol Peruano</translation>
    </message>
    <message>
        <source>Philippine Peso</source>
        <translation type="vanished">Peso filipino</translation>
    </message>
    <message>
        <source>Pakistani Rupee</source>
        <translation type="vanished">Rupia pakistaní</translation>
    </message>
    <message>
        <source>Polish Zloty</source>
        <translation type="vanished">Zloty polaco</translation>
    </message>
    <message>
        <source>Paraguayan Guaran</source>
        <translation type="vanished">Guaran paraguayo</translation>
    </message>
    <message>
        <source>Qatari Riyal</source>
        <translation type="vanished">Riyal catarí</translation>
    </message>
    <message>
        <source>New Romanian Leu</source>
        <translation type="vanished">Nuevo Leu rumano</translation>
    </message>
    <message>
        <source>Russian Rouble</source>
        <translation type="vanished">Rublo ruso</translation>
    </message>
    <message>
        <source>Saudi Riyal</source>
        <translation type="vanished">Riyal saudí</translation>
    </message>
    <message>
        <source>Swedish Krona</source>
        <translation type="vanished">Corona sueca</translation>
    </message>
    <message>
        <source>Singapore Dollar</source>
        <translation type="vanished">Dólar de Singapur</translation>
    </message>
    <message>
        <source>Thai Baht</source>
        <translation type="vanished">Baht tailandés</translation>
    </message>
    <message>
        <source>Tunisian Dinar</source>
        <translation type="vanished">Dinar tunecino</translation>
    </message>
    <message>
        <source>New Turkish Lira</source>
        <translation type="vanished">Nueva lira turca</translation>
    </message>
    <message>
        <source>T&amp;T Dollar (TTD)</source>
        <translation type="vanished">Dólar T&amp;T (TTD)</translation>
    </message>
    <message>
        <source>Taiwan Dollar</source>
        <translation type="vanished">Dólar taiwanés</translation>
    </message>
    <message>
        <source>Ukrainian Hryvnia</source>
        <translation type="vanished">Grivna ucraniana</translation>
    </message>
    <message>
        <source>Uruguayan Peso</source>
        <translation type="vanished">Peso Uruguayo</translation>
    </message>
    <message>
        <source>Venezuelan Bolívar</source>
        <translation type="vanished">Bolívar venezolano</translation>
    </message>
    <message>
        <source>South African Rand</source>
        <translation type="vanished">Rand sudafricano</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="504"/>
        <source>Error!</source>
        <translation>¡Error!</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>Opciones</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>Estándar</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>Científico</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>Tipo de cambio</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>Programador</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>Renunciar</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>Versión: </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>Calculator es una calculadora ligera basada en Qt5, que proporciona cálculo estándar, cálculo científico y conversión de tipo de cambio.</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>Calculadora</translation>
    </message>
</context>
</TS>
