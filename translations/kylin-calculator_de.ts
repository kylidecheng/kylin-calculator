<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>Der Ausdruck ist leer!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>Ausdrucksfehler!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>Linke Klammer fehlt!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>Der Wert ist zu groß!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>Fräulein Operandin!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>Operator undefiniert!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>Der Divisor darf nicht 0 sein!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>Richtiger Operandenfehler!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>Rechner</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>Norm</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>wissenschaftlich</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>Einheiten-Umrechner</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>Wechselkurs</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>Norm</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>wissenschaftlich</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="249"/>
        <source>Calculator</source>
        <translation>Rechner</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1321"/>
        <source>standard</source>
        <translation>Norm</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1303"/>
        <source>calculator</source>
        <translation>Rechner</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="276"/>
        <source>Copy</source>
        <translation>Kopieren</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Paste</source>
        <translation>Kleister</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>input too long</source>
        <translation>Eingabe zu lang</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="77"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>scientific</source>
        <translation>wissenschaftlich</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1338"/>
        <source>exchange rate</source>
        <translation>Wechselkurs</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <location filename="../src/mainwindow.cpp" line="831"/>
        <source>Error!</source>
        <translation>Fehler!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="835"/>
        <source>Input error!</source>
        <translation>Eingabefehler!</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>Eingabe zu lang!</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="86"/>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="87"/>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="88"/>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="89"/>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="232"/>
        <location filename="../src/programmer/programmodel.cpp" line="316"/>
        <source>Input error!</source>
        <translation>Eingabefehler!</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="398"/>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="409"/>
        <source>HideBinary</source>
        <translation>HideBinary</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Calculator</source>
        <translation type="vanished">计算器</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <location filename="../src/scientificmodel.cpp" line="196"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="268"/>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="269"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="271"/>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="272"/>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="273"/>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="274"/>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="276"/>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="277"/>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="278"/>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="279"/>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="281"/>
        <location filename="../src/scientificmodel.cpp" line="1019"/>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="282"/>
        <location filename="../src/scientificmodel.cpp" line="1020"/>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="283"/>
        <location filename="../src/scientificmodel.cpp" line="1021"/>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="284"/>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="286"/>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="287"/>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="288"/>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="289"/>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1012"/>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1013"/>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1014"/>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="288"/>
        <source>Standard</source>
        <translation>Norm</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="289"/>
        <source>Scientific</source>
        <translation>Wissenschaftlich</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="55"/>
        <location filename="../src/titlebar.cpp" line="70"/>
        <location filename="../src/titlebar.cpp" line="256"/>
        <source>standard</source>
        <translation>Norm</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="56"/>
        <location filename="../src/titlebar.cpp" line="268"/>
        <source>scientific</source>
        <translation>wissenschaftlich</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="290"/>
        <source>Exchange Rate</source>
        <translation>Wechselkurs</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Programmer</source>
        <translation>Programmierer</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="316"/>
        <source>StayTop</source>
        <translation>StayTop</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="426"/>
        <source>Restore</source>
        <translation>Wiederherstellen</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="120"/>
        <location filename="../src/titlebar.cpp" line="317"/>
        <source>Minimize</source>
        <translation>Minimieren</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="121"/>
        <location filename="../src/titlebar.cpp" line="318"/>
        <location filename="../src/titlebar.cpp" line="417"/>
        <source>Maximize</source>
        <translation>Maximieren</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="122"/>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="63"/>
        <location filename="../src/programmer/toolbar.cpp" line="207"/>
        <location filename="../src/programmer/toolbar.cpp" line="210"/>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="208"/>
        <location filename="../src/programmer/toolbar.cpp" line="209"/>
        <source>HideBinary</source>
        <translation>HideBinary</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="82"/>
        <source>Rate update</source>
        <translation>Raten-Update</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="97"/>
        <source>Chinese Yuan</source>
        <translation>Chinesischer Yuan</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="104"/>
        <source>US Dollar</source>
        <translation>US-Dollar</translation>
    </message>
    <message>
        <source>UAE Dirham</source>
        <translation type="vanished">VAE-Dirham</translation>
    </message>
    <message>
        <source>Argentinian peso</source>
        <translation type="vanished">Argentinischer Peso</translation>
    </message>
    <message>
        <source>Australian Dollar</source>
        <translation type="vanished">Australischer Dollar</translation>
    </message>
    <message>
        <source>Bulgarian Lev</source>
        <translation type="vanished">Bulgarischer Lew</translation>
    </message>
    <message>
        <source>Bahraini Dinar</source>
        <translation type="vanished">Bahrainischer Dinar</translation>
    </message>
    <message>
        <source>Brunei Dollar</source>
        <translation type="vanished">Brunei-Dollar</translation>
    </message>
    <message>
        <source>Brazilian Real</source>
        <translation type="vanished">Brasilianischer Real</translation>
    </message>
    <message>
        <source>Bahaman Dollar</source>
        <translation type="vanished">Bahama-Dollar</translation>
    </message>
    <message>
        <source>Botswana Pula</source>
        <translation type="vanished">Botswana Pula</translation>
    </message>
    <message>
        <source>Canadian Dollar</source>
        <translation type="vanished">Kanadischer Dollar</translation>
    </message>
    <message>
        <source>CFA Franc</source>
        <translation type="vanished">CFA-Franc</translation>
    </message>
    <message>
        <source>Swiss Franc</source>
        <translation type="vanished">Schweizer Franken</translation>
    </message>
    <message>
        <source>Chilean Peso</source>
        <translation type="vanished">Chilenischer Peso</translation>
    </message>
    <message>
        <source>Colombian Peso</source>
        <translation type="vanished">Kolumbianischer Peso</translation>
    </message>
    <message>
        <source>Czech Koruna</source>
        <translation type="vanished">Tschechische Krone</translation>
    </message>
    <message>
        <source>Danish Krone</source>
        <translation type="vanished">Dänische Krone</translation>
    </message>
    <message>
        <source>Dominican peso</source>
        <translation type="vanished">Dominikanischer Peso</translation>
    </message>
    <message>
        <source>Algerian Dinar</source>
        <translation type="vanished">Algerischer Dinar</translation>
    </message>
    <message>
        <source>Estonian Kroon</source>
        <translation type="vanished">Estnische Krone</translation>
    </message>
    <message>
        <source>Egyptian pound</source>
        <translation type="vanished">Ägyptisches Pfund</translation>
    </message>
    <message>
        <source>Euro</source>
        <translation type="vanished">Euro</translation>
    </message>
    <message>
        <source>Fijian dollar</source>
        <translation type="vanished">Fidschi-Dollar</translation>
    </message>
    <message>
        <source>Pound Sterling</source>
        <translation type="vanished">Pfund Sterling</translation>
    </message>
    <message>
        <source>Guatemalan Quetzal</source>
        <translation type="vanished">Guatemaltekischer Quetzal</translation>
    </message>
    <message>
        <source>Hong Kong Dollar</source>
        <translation type="vanished">Hongkong-Dollar</translation>
    </message>
    <message>
        <source>Croatian Kuna</source>
        <translation type="vanished">Kroatische Kuna</translation>
    </message>
    <message>
        <source>Hungarian Forint</source>
        <translation type="vanished">Ungarischer Forint</translation>
    </message>
    <message>
        <source>Indonesian Rupiah</source>
        <translation type="vanished">Indonesische Rupiah</translation>
    </message>
    <message>
        <source>Israeli New Shekel</source>
        <translation type="vanished">Israelischer Neuer Schekel</translation>
    </message>
    <message>
        <source>Indian Rupee</source>
        <translation type="vanished">Indische Rupie</translation>
    </message>
    <message>
        <source>Iranian Rial</source>
        <translation type="vanished">Iranischer Rial</translation>
    </message>
    <message>
        <source>Icelandic Krona</source>
        <translation type="vanished">Isländische Krone</translation>
    </message>
    <message>
        <source>Japanese Yen</source>
        <translation type="vanished">Japanischer Yen</translation>
    </message>
    <message>
        <source>South Korean Won</source>
        <translation type="vanished">Südkoreanischer Won</translation>
    </message>
    <message>
        <source>Kuwaiti Dinar</source>
        <translation type="vanished">Kuwaitischer Dinar</translation>
    </message>
    <message>
        <source>Kazakhstani Tenge</source>
        <translation type="vanished">Kasachischer Tenge</translation>
    </message>
    <message>
        <source>Sri Lankan Rupee</source>
        <translation type="vanished">Sri-Lanka-Rupie</translation>
    </message>
    <message>
        <source>Lithuanian Litas</source>
        <translation type="vanished">Litauischer Litas</translation>
    </message>
    <message>
        <source>Latvian Lats</source>
        <translation type="vanished">Lettische Lats</translation>
    </message>
    <message>
        <source>Libyan Dinar</source>
        <translation type="vanished">Libyscher Dinar</translation>
    </message>
    <message>
        <source>Mauritian Rupee</source>
        <translation type="vanished">Mauritius-Rupie</translation>
    </message>
    <message>
        <source>Maldivian Rupee</source>
        <translation type="vanished">Maledivische Rupie</translation>
    </message>
    <message>
        <source>Mexican Peso</source>
        <translation type="vanished">Mexikanischer Peso</translation>
    </message>
    <message>
        <source>Malaysian Ringgit</source>
        <translation type="vanished">Malaysischer Ringgit</translation>
    </message>
    <message>
        <source>Norwegian Krone</source>
        <translation type="vanished">Norwegische Krone</translation>
    </message>
    <message>
        <source>Nepalese Rupee</source>
        <translation type="vanished">Nepalesische Rupie</translation>
    </message>
    <message>
        <source>New Zealand Dollar</source>
        <translation type="vanished">Neuseeland-Dollar</translation>
    </message>
    <message>
        <source>Omani Rial</source>
        <translation type="vanished">Omanischer Rial</translation>
    </message>
    <message>
        <source>Panamanian balbos</source>
        <translation type="vanished">Panamaische Balbos</translation>
    </message>
    <message>
        <source>Peruvian Nuevo Sol</source>
        <translation type="vanished">Peruanischer Nuevo Sol</translation>
    </message>
    <message>
        <source>Philippine Peso</source>
        <translation type="vanished">Philippinischer Peso</translation>
    </message>
    <message>
        <source>Pakistani Rupee</source>
        <translation type="vanished">Pakistanische Rupie</translation>
    </message>
    <message>
        <source>Polish Zloty</source>
        <translation type="vanished">Polnischer Zloty</translation>
    </message>
    <message>
        <source>Paraguayan Guaran</source>
        <translation type="vanished">Paraguayischer Guaran</translation>
    </message>
    <message>
        <source>Qatari Riyal</source>
        <translation type="vanished">Katarischer Riyal</translation>
    </message>
    <message>
        <source>New Romanian Leu</source>
        <translation type="vanished">Neue rumänische Leu</translation>
    </message>
    <message>
        <source>Russian Rouble</source>
        <translation type="vanished">Russischer Rubel</translation>
    </message>
    <message>
        <source>Saudi Riyal</source>
        <translation type="vanished">Saudischer Rial</translation>
    </message>
    <message>
        <source>Swedish Krona</source>
        <translation type="vanished">Schwedische Krone</translation>
    </message>
    <message>
        <source>Singapore Dollar</source>
        <translation type="vanished">Singapur-Dollar</translation>
    </message>
    <message>
        <source>Thai Baht</source>
        <translation type="vanished">Thailändische Baht</translation>
    </message>
    <message>
        <source>Tunisian Dinar</source>
        <translation type="vanished">Tunesischer Dinar</translation>
    </message>
    <message>
        <source>New Turkish Lira</source>
        <translation type="vanished">Neue Türkische Lira</translation>
    </message>
    <message>
        <source>T&amp;T Dollar (TTD)</source>
        <translation type="vanished">T&amp;T Dollar (TTD)</translation>
    </message>
    <message>
        <source>Taiwan Dollar</source>
        <translation type="vanished">Taiwan-Dollar</translation>
    </message>
    <message>
        <source>Ukrainian Hryvnia</source>
        <translation type="vanished">Ukrainische Hrywnja</translation>
    </message>
    <message>
        <source>Uruguayan Peso</source>
        <translation type="vanished">Uruguayischer Peso</translation>
    </message>
    <message>
        <source>Venezuelan Bolívar</source>
        <translation type="vanished">Venezolanischer Bolívar</translation>
    </message>
    <message>
        <source>South African Rand</source>
        <translation type="vanished">Südafrikanischer Rand</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="504"/>
        <source>Error!</source>
        <translation>Fehler!</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>Optionen</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>Norm</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>Wissenschaftlich</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>Wechselkurs</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>Programmierer</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>Version: </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>Calculator ist ein leichtgewichtiger Rechner, der auf Qt5 basiert und Standardberechnungen, wissenschaftliche Berechnungen und Wechselkursumrechnungen bietet.</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>Rechner</translation>
    </message>
</context>
</TS>
