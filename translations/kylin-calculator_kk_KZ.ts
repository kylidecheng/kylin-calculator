<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>Calc</name>
    <message>
        <source>Divisor cannot be 0!</source>
        <translation>Бөлінуші 0 бола алмайды!</translation>
    </message>
    <message>
        <source>The expression is empty!</source>
        <translation>Өрнек бос!</translation>
    </message>
    <message>
        <source>Missing left parenthesis!</source>
        <translation>Сол жақ паренхез жоқ!</translation>
    </message>
    <message>
        <source>Miss operand!</source>
        <translation>Операндты жіберіп алдым!</translation>
    </message>
    <message>
        <source>Expression error!</source>
        <translation>Өрнек қатесі!</translation>
    </message>
    <message>
        <source>The value is too large!</source>
        <translation>Мәні тым үлкен!</translation>
    </message>
    <message>
        <source>Operator undefined!</source>
        <translation>Оператор анықталмаған!</translation>
    </message>
    <message>
        <source>Right operand error!</source>
        <translation>Дұрыс жұмыс қатесі!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <source>Unit converter</source>
        <translation>Бірлік түрлендіргіші</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>айырбас бағамы</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>стандартты</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>Калькулятор</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ғылыми</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <source>standard</source>
        <translation>стандартты</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ғылыми</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Copy</source>
        <translation>Көшіру</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Қою</translation>
    </message>
    <message>
        <source>input too long</source>
        <translation>енгізу тым ұзақ</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>айырбас бағамы</translation>
    </message>
    <message>
        <source>Input error!</source>
        <translation>Енгізу қатесі!</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>Қате!</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>стандартты</translation>
    </message>
    <message>
        <source>calculator</source>
        <translation>калькулятор</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>Калькулятор</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ғылыми</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <source>input too long!</source>
        <translation>енгізу тым ұзақ!</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <source>Input error!</source>
        <translation>Енгізу қатесі!</translation>
    </message>
    <message>
        <source>HideBinary</source>
        <translation>HideBinari</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>Кішірейту</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>Айырбас бағамы</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>Барынша көбейту</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>стандартты</translation>
    </message>
    <message>
        <source>StayTop</source>
        <translation>StayTop</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ғылыми</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>Ғылыми</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>Бағдарламашы</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>HideBinary</source>
        <translation>HideBinari</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <source>US Dollar</source>
        <translation>АҚШ доллары</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>Қате!</translation>
    </message>
    <message>
        <source>Rate update</source>
        <translation>Мөлшерлемені жаңарту</translation>
    </message>
    <message>
        <source>Chinese Yuan</source>
        <translation>Қытай Юань</translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <source>cancel</source>
        <translation type="vanished">Болдырмау</translation>
    </message>
    <message>
        <source>currency</source>
        <translation type="vanished">валюта</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Auto</source>
        <translation type="vanished">Авто</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">Қараңғы</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Мәзір</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">Жарық</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тақырып</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>Айырбас бағамы</translation>
    </message>
    <message>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>Калькулятор - стандартты есептеуді, ғылыми есептеуді және айырбас бағамын түрлендіруді қамтамасыз ететін Qt5 негізіндегі жеңілдетілген калькулятор.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">Қызмет және қолдау: </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Нұсқасы: </translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>Калькулятор</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>Ғылыми</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>Бағдарламашы</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
