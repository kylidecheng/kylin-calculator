<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>Calc</name>
    <message>
        <source>Divisor cannot be 0!</source>
        <translation>Бөлүнүүчү 0 болушу мүмкүн эмес!</translation>
    </message>
    <message>
        <source>The expression is empty!</source>
        <translation>Сөз айкашы бош!</translation>
    </message>
    <message>
        <source>Missing left parenthesis!</source>
        <translation>Жок сол парентез!</translation>
    </message>
    <message>
        <source>Miss operand!</source>
        <translation>Мисс операндири!</translation>
    </message>
    <message>
        <source>Expression error!</source>
        <translation>Экспрессия катасы!</translation>
    </message>
    <message>
        <source>The value is too large!</source>
        <translation>Наркы өтө чоң!</translation>
    </message>
    <message>
        <source>Operator undefined!</source>
        <translation>Оператор аныкталбаган!</translation>
    </message>
    <message>
        <source>Right operand error!</source>
        <translation>Туура операндин катасы!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <source>Unit converter</source>
        <translation>Бирдик конвертер</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>алмашуу курсу</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>стандарт</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>Калькулятор</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>илимий</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <source>standard</source>
        <translation>стандарт</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>илимий</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Copy</source>
        <translation>Көчүрмө</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Паста</translation>
    </message>
    <message>
        <source>input too long</source>
        <translation>киргизүү өтө узак</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>алмашуу курсу</translation>
    </message>
    <message>
        <source>Input error!</source>
        <translation>Киргизүү катасы!</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>Ката!</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>стандарт</translation>
    </message>
    <message>
        <source>calculator</source>
        <translation>калькулятор</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>Калькулятор</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>илимий</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <source>input too long!</source>
        <translation>киргизүү өтө узак!</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <source>Input error!</source>
        <translation>Киргизүү катасы!</translation>
    </message>
    <message>
        <source>HideBinary</source>
        <translation>HideBinary</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>Минималдуу</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>Алмашуу курсу</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>Максималдуу</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>стандарт</translation>
    </message>
    <message>
        <source>StayTop</source>
        <translation>StayTop</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>илимий</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>Илимий</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>Программчы</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>HideBinary</source>
        <translation>HideBinary</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ShowBinary</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <source>US Dollar</source>
        <translation>АКШ доллары</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>Ката!</translation>
    </message>
    <message>
        <source>Rate update</source>
        <translation>Ченди жаңылоо</translation>
    </message>
    <message>
        <source>Chinese Yuan</source>
        <translation>Кытай юань</translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <source>cancel</source>
        <translation type="vanished">Жокко чыгаруу</translation>
    </message>
    <message>
        <source>currency</source>
        <translation type="vanished">валюта</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Auto</source>
        <translation type="vanished">Авто</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">Караңгы</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Меню</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Чыгуу</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Жөнүндө</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">Жарык</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Тема</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>Алмашуу курсу</translation>
    </message>
    <message>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>Калькулятор - стандарттык эсептөөнү, илимий эсептөөнү жана алмашуу курсун конверсиялоону камсыз кылган Qt5 негизинде жеңил калькулятор.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">Кызмат &amp;amp: </translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>Версиясы: </translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>Стандарт</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>Калькулятор</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>Илимий</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>Программчы</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
