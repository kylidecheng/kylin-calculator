<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Calc</name>
    <message>
        <source>The expression is empty!</source>
        <translation>མཚོན་པའི་རྣམ་པ།!</translation>
    </message>
    <message>
        <source>Expression error!</source>
        <translation>མཚོན་ཚུལ་ནོར་སྐྱོན།!</translation>
    </message>
    <message>
        <source>Missing left parenthesis!</source>
        <translation>གཡོན་ལྷུང་ཨང་གྲངས་ཆད་སྐྱོན་ཤོར་བ།!</translation>
    </message>
    <message>
        <source>The value is too large!</source>
        <translation>རིན་ཐང་དེ་འདྲ་ཆེ་དྲགས་པ།!</translation>
    </message>
    <message>
        <source>Miss operand!</source>
        <translation>བཀོལ་སྤྱོད་གྲངས་མེད།!</translation>
    </message>
    <message>
        <source>Operator undefined!</source>
        <translation>རྩིས་རྟགས་ལ་མཚན་ཉིད་བཞག་པ།!</translation>
    </message>
    <message>
        <source>Divisor cannot be 0!</source>
        <translation>ཁ་ཤས་ནི་0ཡིན།!</translation>
    </message>
    <message>
        <source>The shifted right operand is negative!</source>
        <translation type="vanished">གཡས་སྤོས་བཀོལ་སྤྱོད་ཀྱི་གྲངས་ཀ་ལ་མཚོན་ན།!</translation>
    </message>
    <message>
        <source>Right operand error!</source>
        <translation>གཡས་སྤོས་བཀོལ་སྤྱོད་ཀྱི་གྲངས་ཀ་ལ་མཚོན་ན།!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <source>Unit converter</source>
        <translation>ཕབ་རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Copy</source>
        <translation>འདྲ་ཕབ།</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>སྦྱར་བ།</translation>
    </message>
    <message>
        <source>input too long</source>
        <translation>ནང་འཇུག་དུས་ཚོད་རིང་བ།</translation>
    </message>
    <message>
        <source>exchange rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>Input error!</source>
        <translation>ནང་འཇུག་ནོར་འཁྲུལ་བྱུང་བ།!</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>ནོར་འཁྲུལ།!</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>calculator</source>
        <translation>རྩིས་ཆས།(_C)</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <source>input too long!</source>
        <translation>ནང་འཇུག་རིང་བ།!</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <source>Input error!</source>
        <translation>ནང་འཇུག་ནོར་འཁྲུལ་བྱུང་བ།!</translation>
    </message>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">ཁྱད་པ་གཉིས་ལྡན་པ།</translation>
    </message>
    <message>
        <source>HideBinary</source>
        <translation>ཏང་གཉིས་དང་།</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ཁྱད་པ་གཉིས་ལྡན་པ།</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་འགྱུར།</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>StayTop</source>
        <translation>མགོར་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
    <message>
        <source>standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>བྱ་རིམ་པ།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ཆེས་ཆེ་བ།</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">ཁྱད་པ་གཉིས་ལྡན་པ།</translation>
    </message>
    <message>
        <source>HideBinary</source>
        <translation>ཏང་གཉིས་དང་།</translation>
    </message>
    <message>
        <source>ShowBinary</source>
        <translation>ཁྱད་པ་གཉིས་ལྡན་པ།</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <source>US Dollar</source>
        <translation>ཨ་སྒོར།</translation>
    </message>
    <message>
        <source>Rate update</source>
        <translation>འཛའ་ཐང་རིམ་འགྱུར།</translation>
    </message>
    <message>
        <source>Chinese Yuan</source>
        <translation>མི་དམངས་ཤོག་སྒོར།</translation>
    </message>
    <message>
        <source>Error!</source>
        <translation>ནོར་འཁྲུལ།!</translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <source>cancel</source>
        <translation type="vanished">ལེན་པ།</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <source>currency</source>
        <translation type="vanished">དངུལ་ལོར།</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Auto</source>
        <translation type="vanished">རང་འགུལ།</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">ནག</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ།</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">འདེམས་བྱང་།</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>About</source>
        <translation>འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">སྟོན་སྒྲོན།</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>བརྗོད་གཞི།</translation>
    </message>
    <message>
        <source>Exchange Rate</source>
        <translation>འཛའ་ཐང་།</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ།：</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation>པར་གཞི།：</translation>
    </message>
    <message>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <source>Calculator</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>Scientific</source>
        <translation>ཚན་རིག</translation>
    </message>
    <message>
        <source>Programmer</source>
        <translation>བྱ་རིམ་པ།</translation>
    </message>
    <message>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>ས་པ་ནི་Qt5ཡི་ཚད་དམའ་བའི་གནས་ཚུལ་ལ་གཞིགས་ཏེ་ཚད་གཞི་ལྟར་རྩིས་རྒྱག་པ་དང་།ཚན་རིག་དང་མཐུན་པ།དངུལ་བརྗེས་འཛའ་ཐང་།.</translation>
    </message>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
