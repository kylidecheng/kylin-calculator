<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="87"/>
        <source>The expression is empty!</source>
        <translation>ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯ ᠨᠢ ᠬᠣᠭᠣᠰᠣᠨ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="109"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="132"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="160"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="188"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="259"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="286"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="314"/>
        <source>Expression error!</source>
        <translation>ᠢᠯᠡᠷᠬᠡᠢᠯᠡᠯ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ᠎ᠲᠠᠢ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="120"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="147"/>
        <source>Missing left parenthesis!</source>
        <translation>ᠵᠡᠭᠦᠨ ᠬᠠᠭᠠᠯᠲᠠ ᠳᠤᠲᠠᠭᠤ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="217"/>
        <source>The value is too large!</source>
        <translation>ᠲᠣᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠬᠡᠲᠦᠷᠬᠡᠢ ᠶᠡᠬᠡ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="230"/>
        <source>Miss operand!</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤ ᠲᠣᠭ᠎ᠠ ᠳᠤᠲᠠᠭᠳᠠᠨ᠎ᠠ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="345"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="404"/>
        <source>Operator undefined!</source>
        <translation>ᠵᠢᠯᠣᠭᠣᠳᠬᠤ ᠲᠡᠮᠳᠡᠭ᠎ᠢ᠋ ᠲᠣᠳᠣᠷᠬᠠᠢᠯᠠᠭᠰᠠᠨ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="367"/>
        <source>Divisor cannot be 0!</source>
        <translation>ᠬᠤᠪᠢᠶᠠᠭᠴᠢ ᠲᠣᠭ᠎ᠠ ᠨᠢ 0 ᠪᠠᠢᠵᠤ ᠪᠣᠯᠬᠤ ᠦᠭᠡᠢ !</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="387"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="395"/>
        <source>Right operand error!</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠲᠣᠭ᠎ᠠ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>ᠰᠣᠯᠢᠨ ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="145"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="148"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="249"/>
        <source>Calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1087"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1321"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1303"/>
        <source>calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="276"/>
        <source>Copy</source>
        <translation>ᠺᠣᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="277"/>
        <source>Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="931"/>
        <source>input too long</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠤᠯᠲᠠ ᠬᠡᠳᠦ ᠤᠷᠲᠤ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="77"/>
        <location filename="../src/mainwindow.cpp" line="1305"/>
        <location filename="../src/mainwindow.cpp" line="1325"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1338"/>
        <source>exchange rate</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="827"/>
        <location filename="../src/mainwindow.cpp" line="831"/>
        <source>Error!</source>
        <translation>ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="835"/>
        <source>Input error!</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠤᠯᠲᠠ ᠨᠢ ᠬᠡᠳᠦ ᠤᠷᠲᠤ !</translation>
    </message>
</context>
<context>
    <name>ProgramKeyboary</name>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="86"/>
        <source>Move X 1 bit to the right</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="87"/>
        <source>Move X 1 bit to the left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="88"/>
        <source>Move X to the right by y bits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/programmer/programkeyboary.cpp" line="89"/>
        <source>Move X to the left by y bits</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="232"/>
        <location filename="../src/programmer/programmodel.cpp" line="316"/>
        <source>Input error!</source>
        <translation>ᠣᠷᠣᠭᠤᠯᠬᠤ ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="398"/>
        <source>ShowBinary</source>
        <translation>ᠦᠵᠡᠭᠦᠯᠬᠦ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="409"/>
        <source>HideBinary</source>
        <translation>ᠨᠢᠭᠤᠭᠳᠠᠮᠠᠯ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Calculator</source>
        <translation type="vanished">计算器</translation>
    </message>
</context>
<context>
    <name>ScientificModel</name>
    <message>
        <location filename="../src/scientificmodel.cpp" line="196"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="268"/>
        <source>Change some keys to interleaving functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="269"/>
        <source>/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="271"/>
        <source>Calculate the reciprocal of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="272"/>
        <source>Square the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="273"/>
        <source>Calculate the cubic value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="274"/>
        <source>Calculate the displayed value power of the next input value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="276"/>
        <source>Calculate the factorial of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="277"/>
        <source>Square root of the displayed square root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="278"/>
        <source>Cubic representation of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="279"/>
        <source> Calculate the displayed value to the y-th power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="281"/>
        <location filename="../src/scientificmodel.cpp" line="1019"/>
        <source>Calculate the sine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="282"/>
        <location filename="../src/scientificmodel.cpp" line="1020"/>
        <source>Calculate the cosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="283"/>
        <location filename="../src/scientificmodel.cpp" line="1021"/>
        <source>Calculate the tangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="284"/>
        <source>Calculate the index value based on the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="286"/>
        <source>Switch between degrees and arcs (click to switch)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="287"/>
        <source>Input pi (3.141596...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="288"/>
        <source>Enter e (the value of e is e ≈ 2.71828 18284 59...)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="289"/>
        <source>Calculate the natural logarithm of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1012"/>
        <source>Calculate the arcsine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1013"/>
        <source>Calculate the arccosine value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scientificmodel.cpp" line="1014"/>
        <source>Calculate the arctangent value of the displayed value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar.cpp" line="288"/>
        <source>Standard</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ — ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="289"/>
        <source>Scientific</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="55"/>
        <location filename="../src/titlebar.cpp" line="70"/>
        <location filename="../src/titlebar.cpp" line="256"/>
        <source>standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="56"/>
        <location filename="../src/titlebar.cpp" line="268"/>
        <source>scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="290"/>
        <source>Exchange Rate</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ — ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="291"/>
        <source>Programmer</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ — ᠫᠷᠦᠭᠷᠠᠮᠴᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="316"/>
        <source>StayTop</source>
        <translation>ᠣᠷᠣᠢ᠎ᠳ᠋ᠤ᠌ ᠲᠠᠯᠪᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="426"/>
        <source>Restore</source>
        <translation>ᠠᠩᠭᠢᠵᠢᠷᠠᠭᠤᠯᠤᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="120"/>
        <location filename="../src/titlebar.cpp" line="317"/>
        <source>Minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="121"/>
        <location filename="../src/titlebar.cpp" line="318"/>
        <location filename="../src/titlebar.cpp" line="417"/>
        <source>Maximize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠶᠡᠭᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="122"/>
        <location filename="../src/titlebar.cpp" line="319"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="63"/>
        <location filename="../src/programmer/toolbar.cpp" line="207"/>
        <location filename="../src/programmer/toolbar.cpp" line="210"/>
        <source>ShowBinary</source>
        <translation>ᠦᠵᠡᠭᠦᠯᠬᠦ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="208"/>
        <location filename="../src/programmer/toolbar.cpp" line="209"/>
        <source>HideBinary</source>
        <translation>ᠨᠢᠭᠤᠭᠳᠠᠮᠠᠯ ᠬᠣᠶᠠᠷ᠎ᠢ᠋ᠶ᠋ᠠᠷ ᠲᠠᠪᠰᠢᠬᠤ ᠳᠦᠷᠢᠮ</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="82"/>
        <source>Rate update</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠰᠢᠨᠡᠳᠬᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="97"/>
        <source>Chinese Yuan</source>
        <translation>ᠠᠷᠠᠳ᠎ᠤ᠋ᠨ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="104"/>
        <source>US Dollar</source>
        <translation>ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <source>UAE Dirham</source>
        <translation type="vanished">ᠠᠷᠠᠪ᠎ᠤ᠋ᠨ ᠬᠣᠯᠪᠣᠭᠠᠲᠤ ᠠᠬᠠᠮᠠᠳᠲᠤ ᠤᠯᠤᠰ᠎ᠤ᠋ᠨ ᠳ᠋ᠢᠯᠠᠮ</translation>
    </message>
    <message>
        <source>Argentinian peso</source>
        <translation type="vanished">ᠠᠷᠭᠸᠨ᠋ᠲ᠋ᠢᠨ᠎ᠤ᠋ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <source>Australian Dollar</source>
        <translation type="vanished">ᠠᠦ᠋ᠰᠲ᠋ᠷᠠᠯᠢᠶ᠎ᠠ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <source>Bulgarian Lev</source>
        <translation type="vanished">ᠪᠣᠯᠭᠠᠷᠢᠶ᠎ᠠ ᠷᠸᠸᠹ</translation>
    </message>
    <message>
        <source>Bahraini Dinar</source>
        <translation type="vanished">ᠪᠠᠭᠠᠷᠢᠨ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>Brunei Dollar</source>
        <translation type="vanished">ᠪᠷᠦᠨ᠋ᠸᠢ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <source>Brazilian Real</source>
        <translation type="vanished">ᠪᠠᠰᠢᠯᠧᠶᠠᠷ</translation>
    </message>
    <message>
        <source>Bahaman Dollar</source>
        <translation type="vanished">ᠪᠠᠬᠠᠮᠠᠮᠠ ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <source>Botswana Pula</source>
        <translation type="vanished">ᠪᠤᠼᠸᠠᠨᠠᠫᠦ᠋ᠯᠠ</translation>
    </message>
    <message>
        <source>Canadian Dollar</source>
        <translation type="vanished">ᠺᠠᠨᠠᠳᠠ᠎ᠶ᠋ᠢᠨ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <source>CFA Franc</source>
        <translation type="vanished">ᠳᠤᠮᠳᠠᠳᠤ ᠠᠹᠷᠢᠺᠠ᠎ᠶ᠋ᠢᠨ ᠹᠷᠠᠩᠺ</translation>
    </message>
    <message>
        <source>Swiss Franc</source>
        <translation type="vanished">ᠰᠸᠢᠰ᠎ᠦ᠋ᠨ ᠹᠷᠠᠩᠺ</translation>
    </message>
    <message>
        <source>Chilean Peso</source>
        <translation type="vanished">ᠴᠢᠯᠢ ᠪᠢ ᠰᠦᠸᠧ</translation>
    </message>
    <message>
        <source>Colombian Peso</source>
        <translation type="vanished">ᠺᠣᠯᠣᠮᠪᠢᠶ᠎ᠠ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <source>Czech Koruna</source>
        <translation type="vanished">ᠴᠧᠺ ᠺᠯᠠᠩ</translation>
    </message>
    <message>
        <source>Danish Krone</source>
        <translation type="vanished">ᠳ᠋ᠠᠨᠮᠠᠷᠺ ᠺᠷᠤᠨᠠ</translation>
    </message>
    <message>
        <source>Dominican peso</source>
        <translation type="vanished">ᠳᠤᠮᠢᠨᠢᠺᠠ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <source>Algerian Dinar</source>
        <translation type="vanished">ᠠᠯᠵᠧᠷᠢᠶ᠎ᠠ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>Estonian Kroon</source>
        <translation type="vanished">ᠡᠧᠰᠲ᠋ᠤᠨᠢᠶ᠎ᠠ ᠺᠷᠤᠨᠠ</translation>
    </message>
    <message>
        <source>Egyptian pound</source>
        <translation type="vanished">ᠶᠧᠵᠢᠫᠲ᠎ᠦ᠋ᠨ ᠫᠦᠨᠳ᠋</translation>
    </message>
    <message>
        <source>Euro</source>
        <translation type="vanished">ᠧᠦ᠋ᠷᠣᠫᠠ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <source>Fijian dollar</source>
        <translation type="vanished">ᠹᠧᠢ ᠵᠢᠢ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <source>Pound Sterling</source>
        <translation type="vanished">ᠫᠦᠨᠳ᠋</translation>
    </message>
    <message>
        <source>Guatemalan Quetzal</source>
        <translation type="vanished">ᠭᠤᠸᠠᠲ᠋ᠸᠮᠠᠯᠠᠭᠲ᠋ᠠᠢ ᠴᠠᠯᠠᠭᠠᠷ</translation>
    </message>
    <message>
        <source>Hong Kong Dollar</source>
        <translation type="vanished">ᠭᠠᠷᠠᠮ᠎ᠤ᠋ᠨ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <source>Croatian Kuna</source>
        <translation type="vanished">ᠺᠷᠤᠲ᠋ᠢᠶ᠎ᠠ ᠺᠤᠨᠠ</translation>
    </message>
    <message>
        <source>Hungarian Forint</source>
        <translation type="vanished">ᠬᠠᠩᠭᠠᠷᠢ᠎ᠶ᠋ᠢᠨ ᠹᠦ ᠯᠢᠨ</translation>
    </message>
    <message>
        <source>Indonesian Rupiah</source>
        <translation type="vanished">ᠢᠨᠳᠣᠨᠧᠽᠢ ᠷᠦᠫᠠ</translation>
    </message>
    <message>
        <source>Israeli New Shekel</source>
        <translation type="vanished">ᠢᠰᠷᠸᠯ ᠰᠢᠨ᠎ᠡ ᠰᠢᠺᠧᠷ</translation>
    </message>
    <message>
        <source>Indian Rupee</source>
        <translation type="vanished">ᠡᠨᠡᠳᠬᠡᠭ᠎ᠦ᠋ᠨ ᠷᠦᠪᠪᠢ</translation>
    </message>
    <message>
        <source>Iranian Rial</source>
        <translation type="vanished">ᠢᠷᠡᠨ ᠷᠢᠶᠠᠯ</translation>
    </message>
    <message>
        <source>Icelandic Krona</source>
        <translation type="vanished">ᠠᠢᠰᠯᠠᠨᠳ᠋ ᠺᠯᠠᠩ</translation>
    </message>
    <message>
        <source>Japanese Yen</source>
        <translation type="vanished">ᠶᠠᠫᠣᠨ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <source>South Korean Won</source>
        <translation type="vanished">ᠬᠠᠨ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <source>Kuwaiti Dinar</source>
        <translation type="vanished">ᠺᠦᠸᠠᠶᠢᠲ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>Kazakhstani Tenge</source>
        <translation type="vanished">ᠺᠠᠽᠠᠭ ᠲᠠᠨ ᠲᠡᠭᠷᠢ</translation>
    </message>
    <message>
        <source>Sri Lankan Rupee</source>
        <translation type="vanished">ᠰᠷᠢ ᠯᠠᠨᠺᠠ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <source>Lithuanian Litas</source>
        <translation type="vanished">ᠯᠢᠲ᠋ᠣ᠎ᠠ ᠯᠢᠲ᠋ᠣ</translation>
    </message>
    <message>
        <source>Latvian Lats</source>
        <translation type="vanished">ᠯᠠᠲ᠋ᠧᠢᠶ᠎ᠠ ᠯᠠᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <source>Libyan Dinar</source>
        <translation type="vanished">ᠯᠢᠪᠢᠶ᠎ᠠ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>Mauritian Rupee</source>
        <translation type="vanished">ᠮᠠᠦ᠋ᠷᠢᠲ᠋ᠢᠦ᠋ᠰ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <source>Maldivian Rupee</source>
        <translation type="vanished">ᠮᠠᠯᠳᠠᠢᠹ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <source>Mexican Peso</source>
        <translation type="vanished">ᠮᠧᠺᠰᠢᠺᠦ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <source>Malaysian Ringgit</source>
        <translation type="vanished">ᠮᠠᠯᠠᠢᠰᠢᠶ᠎ᠠ ᠷᠢᠨᠲ᠋ᠧ</translation>
    </message>
    <message>
        <source>Norwegian Krone</source>
        <translation type="vanished">ᠨᠤᠷᠸᠠᠢ ᠺᠸᠷᠦ᠋ᠨ</translation>
    </message>
    <message>
        <source>Nepalese Rupee</source>
        <translation type="vanished">ᠪᠠᠯᠪᠤ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <source>New Zealand Dollar</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠽᠢᠯᠠᠨᠳ᠋ ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <source>Omani Rial</source>
        <translation type="vanished">ᠠᠮᠠᠮᠠᠨ ᠷᠢᠶᠠᠯ</translation>
    </message>
    <message>
        <source>Panamanian balbos</source>
        <translation type="vanished">ᠫᠠᠨᠠᠮᠠ ᠫᠣᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <source>Peruvian Nuevo Sol</source>
        <translation type="vanished">ᠫᠧᠷᠥ᠋᠎ᠶ᠋ᠢᠨ ᠰᠢᠨ᠎ᠡ ᠰᠤᠷ</translation>
    </message>
    <message>
        <source>Philippine Peso</source>
        <translation type="vanished">ᠹᠢᠯᠢᠫᠢᠨ᠎ᠦ᠌ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <source>Pakistani Rupee</source>
        <translation type="vanished">ᠫᠠᠺᠢᠰᠲ᠋ᠠᠨ᠎ᠤ᠋ ᠷᠦᠫᠧ</translation>
    </message>
    <message>
        <source>Polish Zloty</source>
        <translation type="vanished">ᠫᠣᠯᠠᠨᠼ ᠷᠣᠯᠲ</translation>
    </message>
    <message>
        <source>Paraguayan Guaran</source>
        <translation type="vanished">ᠫᠠᠷᠠᠭᠤᠧ᠋ ᠷᠠᠨᠢ</translation>
    </message>
    <message>
        <source>Qatari Riyal</source>
        <translation type="vanished">ᠺᠠᠲ᠋ᠠᠷᠢᠶ᠎ᠠ (᠎ᠺᠠᠲ᠋ᠠᠷᠢᠶ᠎ᠠ )</translation>
    </message>
    <message>
        <source>New Romanian Leu</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠷᠤᠮᠠᠨᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <source>Russian Rouble</source>
        <translation type="vanished">ᠣᠷᠣᠰ᠎ᠤ᠋ᠨ ᠷᠦᠪᠯᠢ</translation>
    </message>
    <message>
        <source>Saudi Riyal</source>
        <translation type="vanished">ᠰᠠᠦ᠋ᠲ᠋ᠷᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <source>Swedish Krona</source>
        <translation type="vanished">ᠰᠸᠧᠳᠧᠨ ᠺᠸᠷᠦ᠋ᠨ</translation>
    </message>
    <message>
        <source>Singapore Dollar</source>
        <translation type="vanished">ᠰᠢᠩᠭᠠᠫᠦᠷ ᠶᠤᠸᠠᠨ</translation>
    </message>
    <message>
        <source>Thai Baht</source>
        <translation type="vanished">ᠲᠠᠢ ᠵᠣ᠌</translation>
    </message>
    <message>
        <source>Tunisian Dinar</source>
        <translation type="vanished">ᠲᠦᠨᠢᠰ ᠳ᠋ᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <source>New Turkish Lira</source>
        <translation type="vanished">ᠰᠢᠨ᠎ᠡ ᠲᠤᠷᠴᠢ ᠯᠢᠷᠠ</translation>
    </message>
    <message>
        <source>T&amp;T Dollar (TTD)</source>
        <translation type="vanished">ᠲᠷᠢᠨᠢᠳᠠᠳ᠋ ᠪᠠ ᠲᠤᠪᠠᠭᠤ᠋ ᠳ᠋ᠣᠯᠯᠠᠷ</translation>
    </message>
    <message>
        <source>Taiwan Dollar</source>
        <translation type="vanished">ᠲᠠᠢᠸᠠᠨ᠎ᠤ᠋ ᠵᠣᠭᠣᠰ</translation>
    </message>
    <message>
        <source>Ukrainian Hryvnia</source>
        <translation type="vanished">ᠦᠺᠷᠠᠢᠨ ᠭᠷᠢᠹᠨᠠ</translation>
    </message>
    <message>
        <source>Uruguayan Peso</source>
        <translation type="vanished">ᠦᠷᠤᠢᠭᠠᠧᠠᠢᠶᠢᠨ ᠫᠧᠰᠤ᠋</translation>
    </message>
    <message>
        <source>Venezuelan Bolívar</source>
        <translation type="vanished">ᠸᠧᠨ᠋ᠸᠽᠦ᠋ᠸᠯᠠ ᠪᠤᠯᠢᠸᠠᠷ</translation>
    </message>
    <message>
        <source>South African Rand</source>
        <translation type="vanished">ᠡᠮᠦᠨᠡᠲᠦ ᠠᠹᠷᠢᠺᠠ᠎ᠶ᠋ᠢᠨ ᠷᠠᠨᠳ᠋</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="504"/>
        <source>Error!</source>
        <translation>ᠲᠠᠰᠢᠶᠠᠷᠠᠯ !</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="40"/>
        <source>Options</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="52"/>
        <location filename="../src/menumodule/menumodule.cpp" line="90"/>
        <source>Standard</source>
        <translation>ᠪᠠᠷᠢᠮᠵᠢᠶᠠᠲᠤ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="54"/>
        <location filename="../src/menumodule/menumodule.cpp" line="92"/>
        <source>Scientific</source>
        <translation>ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨ᠎ᠤ᠋ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="94"/>
        <source>Exchange Rate</source>
        <translation>ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="96"/>
        <source>Programmer</source>
        <translation>ᠫᠷᠦᠭᠷᠠᠮᠴᠢ᠎ᠶ᠋ᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="64"/>
        <source>Theme</source>
        <translation>ᠭᠣᠣᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="66"/>
        <location filename="../src/menumodule/menumodule.cpp" line="88"/>
        <source>Help</source>
        <translation>ᠬᠠᠪᠰᠤᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <location filename="../src/menumodule/menumodule.cpp" line="86"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="84"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="104"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ ᠄ </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="107"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ ᠪᠣᠯ qt5᠎ᠤ᠋ ᠨᠡᠭᠡᠭᠡᠬᠦ᠎ᠳ᠋ᠦ᠍ ᠰᠠᠭᠤᠷᠢᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠡᠨ ᠬᠥᠩᠭᠡᠨ ᠳᠡᠰ᠎ᠦ᠋ᠨ ᠪᠣᠳᠣᠭᠤᠷ ᠪᠣᠯᠤᠨ᠎ᠠ ᠂ ᠪᠠᠷᠢᠮᠵᠢᠶ᠎ᠠ ᠪᠣᠳᠣᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢ ᠬᠠᠩᠭᠠᠵᠤ ᠂ ᠰᠢᠨᠵᠢᠯᠡᠬᠦ ᠤᠬᠠᠭᠠᠨᠴᠢ᠎ᠪᠠᠷ ᠪᠣᠳᠣᠬᠤ ᠪᠠ ᠭᠤᠪᠢᠭᠤᠯᠭ᠎ᠠ᠎ᠶ᠋ᠢᠨ ᠨᠣᠷᠮ᠎ᠠ᠎ᠶ᠋ᠢ ᠰᠣᠯᠢᠨ ᠪᠣᠳᠣᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="103"/>
        <source>Calculator</source>
        <translation>ᠪᠣᠳᠣᠭᠤᠷ</translation>
    </message>
</context>
</TS>
