/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "data_warehouse.h"

#include <libkysysinfo.h>

#include <QDebug>

DataWarehouse::DataWarehouse()
{
    init();
}

DataWarehouse::~DataWarehouse() {}

DataWarehouse *DataWarehouse::getInstance(void)
{
    static DataWarehouse *instance = NULL;
    if (instance == NULL) {
        instance = new DataWarehouse();
        return instance;
    }

    return instance;
}

void DataWarehouse::init(void)
{
    this->platform.clear();
    this->getPlatForm();

    this->intelPlug = StartMode::MODE_PLUG;
    this->intelMode = false;
    this->maxInputNum = 35;

    return;
}

void DataWarehouse::getPlatForm(void)
{
    this->platform = "xc-tablet";

    char *projectName = kdk_system_get_projectName();
    if (projectName == NULL) {
        return;
    }

    if (QString(projectName) == QString("V10SP1-Intel") || QString(projectName) == QString("V10SP1-edu")) {
        this->platform = QString("intel");
    }

    free(projectName);

    qDebug() << "Info : check run platform is " << this->platform;

    return;
}
