/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "logic_center.h"

LogicCenter::LogicCenter()
{
    this->init();
    this->establishInterrupt();
}

LogicCenter::~LogicCenter() {}

LogicCenter *LogicCenter::getInstance(void)
{
    static LogicCenter *ptr = nullptr;

    if (ptr == nullptr) {
        ptr = new LogicCenter();
    }

    return ptr;
}

void LogicCenter::init(void)
{
    /* 注意 : 数据仓库需在界面前进行实例 , 界面实例时会访问数据仓库中的数据 */
    DataWarehouse::getInstance();

    /* 走 main 函数 , 说明为正常启动 */
    DataWarehouse::getInstance()->intelPlug = DataWarehouse::StartMode::MODE_MAIN;

    /* 实例主界面 */
    this->m_mainWin = MainWindow::getInstance();

    return;
}

void LogicCenter::establishInterrupt(void)
{
    connect(this->m_mainWin, &MainWindow::sigTranparencyChange, this, &LogicCenter::slotTranparencyChange);

    return;
}

void LogicCenter::slotTranparencyChange(void)
{
    MainWindow::getInstance()->update();

    return;
}
