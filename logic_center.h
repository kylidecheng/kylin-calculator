/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __LOGIC_CENTER__
#define __LOGIC_CENTER__

#include <QObject>

#include "mainwindow.h"
#include "data_warehouse.h"

class LogicCenter : public QObject
{
    Q_OBJECT

public:
    static LogicCenter *getInstance(void);
    ~LogicCenter();

private:
    MainWindow *m_mainWin;

    LogicCenter();
    void init(void);
    void establishInterrupt(void);

signals:

public slots:
    void slotTranparencyChange(void);
};

#endif
