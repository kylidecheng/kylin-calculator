/*
 * Copyright (C) 2023 KylinSoft Co., Ltd.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <QStringList>
#include <QTranslator>
#include <QLocale>
#include <QStandardPaths>
#include <QLibraryInfo>
#include <QDir>
#include <QDebug>
#include <singleapplication.h>

#include "log.hpp"
#include "windowmanage.hpp"

#include "mainwindow.h"
#include "data_warehouse.h"
#include "logic_center.h"

int main(int argc, char *argv[])
{
    qInstallMessageHandler(kabase::Log::logOutput);
    kabase::WindowManage::setScalingProperties();
    kdk::QtSingleApplication a(argc, argv);
    a.setApplicationVersion("1.2.0.2");
    a.setWindowIcon(QIcon::fromTheme("kylin-calculator"));
    if (a.isRunning()) {
        qDebug() << "is running";
        a.sendMessage("running , 4000");
        return 0;
    }

    /* 加载翻译文件 */
    QString tranPath("/usr/share/kylin-calculator/translations/");
    QString tranQtPath = QLibraryInfo::location(QLibraryInfo::TranslationsPath);
    QTranslator *tran = new QTranslator;
    QTranslator *tranQt = new QTranslator;
    if (tran->load(QLocale(), QString("kylin-calculator"), QString("_"), tranPath)) {
        QApplication::installTranslator(tran);
    } else {
        qDebug() << "Waring : load translation file fail";
    }
    if (tranQt->load(QLocale(), QString("qt"), QString("_"), tranQtPath)) {
        QApplication::installTranslator(tranQt);
    } else {
        qDebug() << "Waring : load qt translation file fail";
    }

    // 加载sdk控件翻译
    QTranslator trans;
    QString locale = QLocale::system().name();
    if (locale == "zh_CN") {
        if (trans.load(":/translations/gui_zh_CN.qm")) {
            a.installTranslator(&trans);
        }
    } else if (locale == "bo_CN") {
        if (trans.load(":/translations/gui_bo_CN.qm")) {
            a.installTranslator(&trans);
        }
    }
    /*
     else if (locale == "ug_CN") {
        if (trans.load(":/translations/gui_ug_CN.qm")) {
            a.installTranslator(&trans);
        }
    } else if (locale == "zh_HK") {
        if (trans.load(":/translations/gui_zh_HK.qm")) {
            a.installTranslator(&trans);
        }
    } else if (locale == "kk_KZ") {
        if (trans.load(":/translations/gui_kk_KZ.qm")) {
            a.installTranslator(&trans);
        }
    } else if (locale == "ky_KG") {
        if (trans.load(":/translations/gui_ky_KG.qm")) {
            a.installTranslator(&trans);
        }
    } else if (locale == "mn") {
        if (trans.load(":/translations/gui_mn.qm")) {
            a.installTranslator(&trans);
        }
    }
*/
    /* 实例 */
    LogicCenter::getInstance();

    QString platform = QGuiApplication::platformName();
    if(platform.startsWith(QLatin1String("Wayland"),Qt::CaseInsensitive) || QString(qgetenv("XDG_SESSION_TYPE")) == "wayland") {
        MainWindow::getInstance()->setIsWayland(true);
    }

    /* 添加窗管协议 */
    kabase::WindowManage::removeHeader(MainWindow::getInstance());

    /* 移动到窗口中间 */
    // QRect availableGeometry = qApp->primaryScreen()->availableGeometry();
    // MainWindow::getInstance()->move(availableGeometry.width() / 2 - MainWindow::getInstance()->width() / 2,
    //                                 availableGeometry.height() / 2 - MainWindow::getInstance()->height() / 2);

    a.setActivationWindow(MainWindow::getInstance());

    /* wayland 下最小化拉起 */
    if (QGuiApplication::platformName().startsWith(QLatin1String("wayland"), Qt::CaseInsensitive)) {
        QObject::connect(&a, &kdk::QtSingleApplication::messageReceived, [=]() {
            kabase::WindowManage::activateWindow(MainWindow::getInstance()->getWinId());
        });
    }

    MainWindow::getInstance()->show();
    if (MainWindow::getInstance()->isWayland() && MainWindow::getInstance()->isTabletMode()) {
        MainWindow::getInstance()->setWindowState(MainWindow::getInstance()->windowState() | Qt::WindowFullScreen);
    }
    else {
        kabase::WindowManage::setMiddleOfScreen(MainWindow::getInstance());
    }

    return a.exec();
}
